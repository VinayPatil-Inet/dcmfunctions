/****** Object:  Database [dcmdatabase]    Script Date: 15-12-2020 10:35:53 ******/
CREATE DATABASE [dcmdatabase]  (EDITION = 'Standard', SERVICE_OBJECTIVE = 'S0', MAXSIZE = 5 GB) WITH CATALOG_COLLATION = SQL_Latin1_General_CP1_CI_AS;
GO
ALTER DATABASE [dcmdatabase] SET COMPATIBILITY_LEVEL = 150
GO
ALTER DATABASE [dcmdatabase] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [dcmdatabase] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [dcmdatabase] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [dcmdatabase] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [dcmdatabase] SET ARITHABORT OFF 
GO
ALTER DATABASE [dcmdatabase] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [dcmdatabase] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [dcmdatabase] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [dcmdatabase] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [dcmdatabase] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [dcmdatabase] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [dcmdatabase] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [dcmdatabase] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [dcmdatabase] SET ALLOW_SNAPSHOT_ISOLATION ON 
GO
ALTER DATABASE [dcmdatabase] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [dcmdatabase] SET READ_COMMITTED_SNAPSHOT ON 
GO
ALTER DATABASE [dcmdatabase] SET  MULTI_USER 
GO
ALTER DATABASE [dcmdatabase] SET ENCRYPTION ON
GO
ALTER DATABASE [dcmdatabase] SET QUERY_STORE = ON
GO
ALTER DATABASE [dcmdatabase] SET QUERY_STORE (OPERATION_MODE = READ_WRITE, CLEANUP_POLICY = (STALE_QUERY_THRESHOLD_DAYS = 30), DATA_FLUSH_INTERVAL_SECONDS = 900, INTERVAL_LENGTH_MINUTES = 60, MAX_STORAGE_SIZE_MB = 100, QUERY_CAPTURE_MODE = AUTO, SIZE_BASED_CLEANUP_MODE = AUTO, MAX_PLANS_PER_QUERY = 200, WAIT_STATS_CAPTURE_MODE = ON)
GO
/*** The scripts of database scoped configurations in Azure should be executed inside the target database connection. ***/
GO
-- ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 8;
GO
/****** Object:  Table [dbo].[bcbsri_risk_report_tbl]    Script Date: 15-12-2020 10:35:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bcbsri_risk_report_tbl](
	[bcbsri_risk_report_id] [int] IDENTITY(1,1) NOT NULL,
	[BCBSRI_Risk_Categorization] [varchar](150) NULL,
	[Prospective_Risk_Score] [varchar](150) NULL,
	[RUB] [varchar](150) NULL,
	[Adv_Dir_S0257] [varchar](150) NULL,
	[Asthma] [int] NULL,
	[BCBSRI_ID] [varchar](250) NULL,
	[BH_CM_Discharge_Date] [varchar](150) NULL,
	[BH_CM_Discharge_Reason] [varchar](max) NULL,
	[BH_CM_Enrollment] [varchar](150) NULL,
	[BH_Risk_Category] [varchar](150) NULL,
	[Cancer] [int] NULL,
	[CHF] [int] NULL,
	[CKD] [varchar](150) NULL,
	[CONSISTENT_MEMBER_ID] [varchar](max) NULL,
	[contracted_group_name] [varchar](150) NULL,
	[COPD] [int] NULL,
	[Date_Attributed_to_PCP] [int] NULL,
	[Depression] [int] NULL,
	[Diabetes] [int] NULL,
	[ESRD] [int] NULL,
	[HCBB_Eligible] [varchar](max) NULL,
	[HCBB_Engaged] [varchar](250) NULL,
	[High_Cost_50K] [varchar](150) NULL,
	[High_Cost_Driver] [varchar](150) NULL,
	[Hospice] [int] NULL,
	[Hyperlipid] [int] NULL,
	[Hypertension] [int] NULL,
	[In_Home_Assessment_Status] [varchar](max) NULL,
	[In_Home_Assessment_Status_Date] [varchar](150) NULL,
	[IschemicHD] [int] NULL,
	[LowBackPain] [int] NULL,
	[Mbr_Addr1] [varchar](max) NULL,
	[Mbr_Addr2] [varchar](max) NULL,
	[Mbr_Age] [varchar](50) NULL,
	[Mbr_City] [varchar](150) NULL,
	[Mbr_DOB] [varchar](150) NULL,
	[Mbr_First_Name] [varchar](150) NULL,
	[Mbr_Gender] [varchar](150) NULL,
	[Mbr_Last_Name] [varchar](150) NULL,
	[Mbr_Phone_Nbr] [varchar](150) NULL,
	[Mbr_State] [varchar](150) NULL,
	[Mbr_Zip] [varchar](150) NULL,
	[Medical_Cost] [varchar](150) NULL,
	[Medicare_Dual_Coverage_Type] [varchar](150) NULL,
	[Medicare_ID] [varchar](max) NULL,
	[Medicare_Risk_Index] [varchar](150) NULL,
	[IP_Medical_Cnt] [varchar](150) NULL,
	[New_PCMH_HR_Flag] [varchar](150) NULL,
	[OP_ER_Cnt] [varchar](150) NULL,
	[PCP_First_name] [varchar](max) NULL,
	[PCP_Last_Name] [varchar](max) NULL,
	[Perf_Guarantee_Mbr] [varchar](150) NULL,
	[Pharmacy_Cost] [nvarchar](50) NULL,
	[Practice_Site] [varchar](max) NULL,
	[Probability_of_IP_in_6mos] [varchar](max) NULL,
	[Product] [varchar](150) NULL,
	[Requires_PCP_Referral] [varchar](150) NULL,
	[RxSpecialty_Disease_Desc] [varchar](max) NULL,
	[RxSpecialty_Drug] [varchar](150) NULL,
	[Total_Cost] [varchar](150) NULL,
	[status] [varchar](50) NULL,
	[inserted_date] [datetime] NULL,
	[updated_date] [datetime] NULL,
	[insrted_by] [int] NULL,
	[updated_by] [int] NULL,
	[report_date] [date] NULL,
	[pcmh_id] [int] NULL,
	[for_month] [int] NULL,
	[for_year] [varchar](50) NULL,
	[type_of_sheet] [int] NULL,
 CONSTRAINT [PK_bcbsri_risk_report_tbl] PRIMARY KEY CLUSTERED 
(
	[bcbsri_risk_report_id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[bcbsriprogram_data_tbl]    Script Date: 15-12-2020 10:35:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[bcbsriprogram_data_tbl](
	[bcbsriprogram_data_id] [int] IDENTITY(1,1) NOT NULL,
	[uploadedfileid] [int] NULL,
	[Mbr_Last_Name] [varchar](250) NULL,
	[Mbr_First_Name] [varchar](250) NULL,
	[CONSISTENT_MEMBER_ID] [varchar](250) NULL,
	[BCBSRI_ID] [varchar](250) NULL,
	[Mbr_DOB] [varchar](250) NULL,
	[BCBSRI_Risk_Categorization] [varchar](150) NULL,
	[In_Home_Assessment_Status] [varchar](250) NULL,
	[In_Home_Assessment_Status_date] [varchar](250) NULL,
	[BH_CM_Flag] [varchar](250) NULL,
	[BH_CM_Discharge_Dt] [varchar](250) NULL,
	[BH_CM_Discharge_Reason] [varchar](250) NULL,
	[HCBB_Eligible] [varchar](250) NULL,
	[HCBB_Engaged] [varchar](250) NULL,
	[inserteddate] [nchar](10) NULL,
	[for_month] [int] NULL,
	[for_year] [varchar](50) NULL,
	[inserted_by] [int] NULL,
	[pcmh_id] [int] NULL,
	[monthname] [varchar](50) NULL,
 CONSTRAINT [PK_bcbsriprogram_data_tbl] PRIMARY KEY CLUSTERED 
(
	[bcbsriprogram_data_id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[conditionsandrisk_data_tbl]    Script Date: 15-12-2020 10:35:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[conditionsandrisk_data_tbl](
	[conditionsandrisk_data_id] [int] IDENTITY(1,1) NOT NULL,
	[uploadedfileid] [int] NULL,
	[Mbr_Last_Name] [varchar](250) NULL,
	[Mbr_First_Name] [varchar](250) NULL,
	[CONSISTENT_MEMBER_ID] [varchar](250) NULL,
	[BCBSRI_ID] [varchar](250) NULL,
	[Mbr_DOB] [varchar](250) NULL,
	[BCBSRI_Risk_Categorization] [varchar](150) NULL,
	[New_PCMH_HR_Flag] [varchar](250) NULL,
	[RUB] [varchar](250) NULL,
	[Medicare_Risk_Index] [varchar](250) NULL,
	[Hypertension] [int] NULL,
	[Hyperlipid] [int] NULL,
	[LowBackPain] [int] NULL,
	[Diabetes] [int] NULL,
	[IschemicHD] [int] NULL,
	[Asthma] [int] NULL,
	[COPD] [int] NULL,
	[CHF] [int] NULL,
	[Cancer] [int] NULL,
	[Depression] [int] NULL,
	[ESRD] [int] NULL,
	[CKD] [varchar](250) NULL,
	[Hospice_Flag] [varchar](250) NULL,
	[BH_Risk_Category] [varchar](250) NULL,
	[Adv_Dir_S0257] [varchar](150) NULL,
	[inserteddate] [nchar](10) NULL,
	[for_month] [int] NULL,
	[for_year] [varchar](50) NULL,
	[inserted_by] [int] NULL,
	[pcmh_id] [int] NULL,
	[monthname] [varchar](50) NULL,
 CONSTRAINT [PK_conditionsandrisk_data_tbl] PRIMARY KEY CLUSTERED 
(
	[conditionsandrisk_data_id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[costandutilization_data_tbl]    Script Date: 15-12-2020 10:35:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[costandutilization_data_tbl](
	[costandutilization_data_id] [int] IDENTITY(1,1) NOT NULL,
	[uploadedfileid] [int] NULL,
	[Mbr_Last_Name] [varchar](250) NULL,
	[Mbr_First_Name] [varchar](250) NULL,
	[CONSISTENT_MEMBER_ID] [varchar](250) NULL,
	[BCBSRI_ID] [varchar](250) NULL,
	[Mbr_DOB] [varchar](250) NULL,
	[BCBSRI_Risk_Categorization] [varchar](150) NULL,
	[Probability_of_IP_in_6mos] [varchar](250) NULL,
	[IP_Medical_Cnt] [varchar](250) NULL,
	[OP_ER_Cnt] [varchar](250) NULL,
	[Total_Cost] [varchar](250) NULL,
	[Medical_Cost] [varchar](250) NULL,
	[Rx_Cost] [varchar](250) NULL,
	[High_Cost_50k] [varchar](250) NULL,
	[High_Cost_Driver] [varchar](250) NULL,
	[RxSpecialty_Drug] [varchar](250) NULL,
	[RxSpecialty_Disease_Desc] [varchar](250) NULL,
	[inserteddate] [nchar](10) NULL,
	[for_month] [int] NULL,
	[for_year] [varchar](50) NULL,
	[inserted_by] [int] NULL,
	[pcmh_id] [int] NULL,
	[monthname] [varchar](50) NULL,
 CONSTRAINT [PK_costandutilization_data_tbl] PRIMARY KEY CLUSTERED 
(
	[costandutilization_data_id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[demographic_data_tbl]    Script Date: 15-12-2020 10:35:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[demographic_data_tbl](
	[demographic_data_id] [int] IDENTITY(1,1) NOT NULL,
	[uploadedfileid] [int] NULL,
	[Mbr_Last_Name] [varchar](250) NULL,
	[Mbr_First_Name] [varchar](250) NULL,
	[CONSISTENT_MEMBER_ID] [varchar](250) NULL,
	[BCBSRI_ID] [varchar](250) NULL,
	[Mbr_DOB] [varchar](250) NULL,
	[Mbr_Age] [varchar](150) NULL,
	[Mbr_Gender] [varchar](250) NULL,
	[BCBSRI_Risk_Categorization] [varchar](250) NULL,
	[New_PCMH_HR_Flag] [varchar](250) NULL,
	[Perf_Guarantee_Mbr] [varchar](250) NULL,
	[contracted_group_name] [varchar](250) NULL,
	[Practice_Site] [varchar](250) NULL,
	[PCP_Last_Name] [varchar](250) NULL,
	[PCP_First_name] [varchar](250) NULL,
	[Last_PCP_Visit_dt] [varchar](250) NULL,
	[Product] [varchar](250) NULL,
	[Requires_PCP_Referral] [varchar](250) NULL,
	[Medicare_Dual_Coverage_Type] [varchar](250) NULL,
	[Mbr_Addr1] [varchar](500) NULL,
	[Mbr_Addr2] [varchar](500) NULL,
	[Mbr_City] [varchar](250) NULL,
	[Mbr_State] [varchar](250) NULL,
	[Mbr_Zip] [varchar](250) NULL,
	[Mbr_Phone_Nbr] [varchar](150) NULL,
	[inserteddate] [nchar](10) NULL,
	[for_month] [int] NULL,
	[for_year] [varchar](50) NULL,
	[inserted_by] [int] NULL,
	[pcmh_id] [int] NULL,
	[monthname] [varchar](50) NULL,
 CONSTRAINT [PK_demographic_data_tbl] PRIMARY KEY CLUSTERED 
(
	[demographic_data_id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[patientalldata_tbl]    Script Date: 15-12-2020 10:35:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[patientalldata_tbl](
	[patientalldata_id] [int] IDENTITY(1,1) NOT NULL,
	[uploadedfileid] [int] NULL,
	[BCBSRI_Risk_Categorization] [varchar](150) NULL,
	[Prospective_Risk_Score] [varchar](150) NULL,
	[RUB] [varchar](150) NULL,
	[Adv_Dir_S0257] [varchar](150) NULL,
	[Asthma] [int] NULL,
	[BCBSRI_ID] [varchar](250) NULL,
	[BH_CM_Discharge_Date] [varchar](150) NULL,
	[BH_CM_Discharge_Reason] [varchar](max) NULL,
	[BH_CM_Enrollment] [varchar](150) NULL,
	[BH_Risk_Category] [varchar](150) NULL,
	[Cancer] [int] NULL,
	[CHF] [int] NULL,
	[CKD] [varchar](150) NULL,
	[CONSISTENT_MEMBER_ID] [varchar](max) NULL,
	[contracted_group_name] [varchar](150) NULL,
	[COPD] [int] NULL,
	[Date_Attributed_to_PCP] [int] NULL,
	[Depression] [int] NULL,
	[Diabetes] [int] NULL,
	[ESRD] [int] NULL,
	[HCBB_Eligible] [varchar](max) NULL,
	[HCBB_Engaged] [varchar](250) NULL,
	[High_Cost_50K] [varchar](150) NULL,
	[High_Cost_Driver] [varchar](150) NULL,
	[Hospice] [int] NULL,
	[Hyperlipid] [int] NULL,
	[Hypertension] [int] NULL,
	[In_Home_Assessment_Status] [varchar](max) NULL,
	[In_Home_Assessment_Status_Date] [varchar](150) NULL,
	[IschemicHD] [int] NULL,
	[LowBackPain] [int] NULL,
	[Mbr_Addr1] [varchar](max) NULL,
	[Mbr_Addr2] [varchar](max) NULL,
	[Mbr_Age] [varchar](50) NULL,
	[Mbr_City] [varchar](150) NULL,
	[Mbr_DOB] [varchar](150) NULL,
	[Mbr_First_Name] [varchar](150) NULL,
	[Mbr_Gender] [varchar](150) NULL,
	[Mbr_Last_Name] [varchar](150) NULL,
	[Mbr_Phone_Nbr] [varchar](150) NULL,
	[Mbr_State] [varchar](150) NULL,
	[Mbr_Zip] [varchar](150) NULL,
	[Medical_Cost] [varchar](150) NULL,
	[Medicare_Dual_Coverage_Type] [varchar](150) NULL,
	[Medicare_ID] [varchar](max) NULL,
	[Medicare_Risk_Index] [varchar](150) NULL,
	[IP_Medical_Cnt] [varchar](150) NULL,
	[New_PCMH_HR_Flag] [varchar](150) NULL,
	[OP_ER_Cnt] [varchar](150) NULL,
	[PCP_First_name] [varchar](max) NULL,
	[PCP_Last_Name] [varchar](max) NULL,
	[Perf_Guarantee_Mbr] [varchar](150) NULL,
	[Pharmacy_Cost] [varchar](50) NULL,
	[Practice_Site] [varchar](max) NULL,
	[Probability_of_IP_in_6mos] [varchar](max) NULL,
	[Product] [varchar](150) NULL,
	[Requires_PCP_Referral] [varchar](150) NULL,
	[RxSpecialty_Disease_Desc] [varchar](max) NULL,
	[RxSpecialty_Drug] [varchar](150) NULL,
	[Total_Cost] [varchar](150) NULL,
	[inserteddate] [datetime] NULL,
	[updated_date] [varchar](50) NULL,
	[insrted_by] [int] NULL,
	[updated_by] [int] NULL,
	[pcmh_id] [int] NULL,
	[for_month] [int] NULL,
	[for_year] [varchar](50) NULL,
	[status] [varchar](50) NULL,
	[report_date] [varchar](50) NULL,
	[monthname] [varchar](50) NULL,
 CONSTRAINT [PK_patientalldata_tbl] PRIMARY KEY CLUSTERED 
(
	[patientalldata_id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[pcmh_tbl]    Script Date: 15-12-2020 10:35:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pcmh_tbl](
	[pcmhid] [int] IDENTITY(1,1) NOT NULL,
	[pcmhname] [varchar](50) NULL,
	[pcmhsitename] [varchar](50) NULL,
	[pcmhNPI_ID] [varchar](max) NULL,
	[pcmhTax_ID] [varchar](max) NULL,
	[pcmh_dgn_username] [varchar](50) NULL,
	[firstname] [varchar](max) NULL,
	[lastname] [varchar](max) NULL,
	[emailID] [varchar](50) NULL,
	[pcmhuserid] [varchar](50) NULL,
	[password] [varchar](max) NULL,
	[inserteddate] [datetime] NOT NULL,
	[isactive] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[reportstatus_tbl]    Script Date: 15-12-2020 10:35:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[reportstatus_tbl](
	[reportstatus_id] [bigint] NULL,
	[returnreportid] [int] NULL,
	[bcbsri_risk_report_id] [int] NULL,
	[status] [varchar](50) NULL,
	[for_month] [int] NULL,
	[for_year] [varchar](50) NULL,
	[inserteddate] [timestamp] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[return_report_tbl]    Script Date: 15-12-2020 10:35:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[return_report_tbl](
	[returnreportid] [bigint] NULL,
	[bcbsri_risk_report_id] [int] NULL,
	[uploadedfileid] [int] NULL,
	[userid] [int] NULL,
	[outreach_attempted_date] [varchar](50) NULL,
	[enrolled_status_date] [varchar](50) NULL,
	[BHscreening_PHQ2_date] [varchar](50) NULL,
	[cureplan_established_date] [varchar](50) NULL,
	[dischargedfrom_CM_date] [varchar](50) NULL,
	[for_month] [int] NULL,
	[for_year] [varchar](50) NULL,
	[inserteddate] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[returnreport_data_tbl]    Script Date: 15-12-2020 10:35:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[returnreport_data_tbl](
	[returnreport_data_id] [int] IDENTITY(1,1) NOT NULL,
	[uploadedfileid] [int] NULL,
	[Mbr_Last_Name] [varchar](250) NULL,
	[Mbr_First_Name] [varchar](250) NULL,
	[BCBSRI_ID] [varchar](250) NULL,
	[Mbr_DOB] [varchar](250) NULL,
	[BCBSRI_Risk_Categorization] [varchar](150) NULL,
	[Perf_Guarantee_Mbr] [varchar](250) NULL,
	[Practice_Site] [varchar](250) NULL,
	[Practice_Identified_Indicator] [varchar](250) NULL,
	[Outreach_Attempted_Date] [varchar](150) NULL,
	[Enrolled_Status_Date] [varchar](150) NULL,
	[BH_Screening_PHQ2_PHQ9_Completed_Date] [varchar](150) NULL,
	[Care_Plan_Established_Date] [varchar](150) NULL,
	[Discharged_from_CM_Date] [varchar](150) NULL,
	[Status] [varchar](250) NULL,
	[inserteddate] [nchar](10) NULL,
	[for_month] [int] NULL,
	[for_year] [varchar](50) NULL,
	[inserted_by] [int] NULL,
	[updated_by] [int] NULL,
	[currentstatus] [int] NULL,
	[pcmh_id] [int] NULL,
	[monthname] [varchar](50) NULL,
 CONSTRAINT [PK_returnreport_data_tbl] PRIMARY KEY CLUSTERED 
(
	[returnreport_data_id] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Table_1]    Script Date: 15-12-2020 10:35:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Table_1](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DATE] [datetime] NULL,
	[ISUPDATED] [bit] NULL,
	[name] [varchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[uploadedfiles_tbl]    Script Date: 15-12-2020 10:35:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[uploadedfiles_tbl](
	[uploadedfileid] [int] IDENTITY(1,1) NOT NULL,
	[filename] [varchar](max) NULL,
	[userid] [int] NULL,
	[for_month] [varchar](50) NULL,
	[for_year] [varchar](50) NULL,
	[status] [varchar](50) NULL,
	[iscompleted] [int] NULL,
	[inserteddate] [varchar](50) NULL,
	[pcmh_id] [int] NULL,
	[monthname] [varchar](50) NULL,
 CONSTRAINT [PK_uploadedfiles_tbl] PRIMARY KEY CLUSTERED 
(
	[uploadedfileid] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_tbl]    Script Date: 15-12-2020 10:35:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_tbl](
	[userid] [int] IDENTITY(1,1) NOT NULL,
	[pcmhid] [int] NULL,
	[usertypeid] [int] NULL,
	[password] [varchar](max) NULL,
	[username] [nvarchar](50) NULL,
	[address] [nvarchar](max) NULL,
	[email] [nvarchar](50) NULL,
	[phone] [nvarchar](50) NULL,
	[inserteddate] [varchar](50) NULL,
	[isactive] [bit] NULL,
	[firstname] [varchar](max) NULL,
	[lastname] [varchar](max) NULL,
 CONSTRAINT [PK_user_tbl] PRIMARY KEY CLUSTERED 
(
	[userid] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[usertype_tbl]    Script Date: 15-12-2020 10:35:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[usertype_tbl](
	[usertypeid] [int] IDENTITY(1,1) NOT NULL,
	[usertype] [nvarchar](50) NULL,
	[issuperadmin] [bit] NULL,
	[inserteddate] [datetime] NULL,
 CONSTRAINT [PK_usertype_tbl] PRIMARY KEY CLUSTERED 
(
	[usertypeid] ASC
)WITH (STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[bcbsri_risk_report_tbl] ADD  CONSTRAINT [DF_bcbsri_risk_report_tbl_inserted_date]  DEFAULT (getdate()) FOR [inserted_date]
GO
ALTER TABLE [dbo].[bcbsriprogram_data_tbl] ADD  CONSTRAINT [DF_bcbsriprogram_data_tbl_inserteddate]  DEFAULT (getdate()) FOR [inserteddate]
GO
ALTER TABLE [dbo].[conditionsandrisk_data_tbl] ADD  CONSTRAINT [DF_conditionsandrisk_data_tbl_inserteddate]  DEFAULT (getdate()) FOR [inserteddate]
GO
ALTER TABLE [dbo].[costandutilization_data_tbl] ADD  CONSTRAINT [DF_costandutilization_data_tbl_inserteddate]  DEFAULT (getdate()) FOR [inserteddate]
GO
ALTER TABLE [dbo].[demographic_data_tbl] ADD  CONSTRAINT [DF_demographic_data_tbl_inserteddate]  DEFAULT (getdate()) FOR [inserteddate]
GO
ALTER TABLE [dbo].[patientalldata_tbl] ADD  CONSTRAINT [DF_patientalldata_tbl_inserted_date]  DEFAULT (getdate()) FOR [inserteddate]
GO
ALTER TABLE [dbo].[pcmh_tbl] ADD  CONSTRAINT [DF_pcmh_tbl_inserteddate]  DEFAULT (getdate()) FOR [inserteddate]
GO
ALTER TABLE [dbo].[pcmh_tbl] ADD  CONSTRAINT [DF_pcmh_tbl_isactive]  DEFAULT ((1)) FOR [isactive]
GO
ALTER TABLE [dbo].[return_report_tbl] ADD  CONSTRAINT [DF_return_report_tbl_inserteddate]  DEFAULT (getdate()) FOR [inserteddate]
GO
ALTER TABLE [dbo].[returnreport_data_tbl] ADD  CONSTRAINT [DF_returnreport_data_tbl_inserteddate]  DEFAULT (getdate()) FOR [inserteddate]
GO
ALTER TABLE [dbo].[returnreport_data_tbl] ADD  CONSTRAINT [DF_returnreport_data_tbl_currentstatus]  DEFAULT ((0)) FOR [currentstatus]
GO
ALTER TABLE [dbo].[Table_1] ADD  CONSTRAINT [DF_Table_1_DATE]  DEFAULT (getdate()) FOR [DATE]
GO
ALTER TABLE [dbo].[Table_1] ADD  CONSTRAINT [DF_Table_1_ISUPDATED]  DEFAULT ((0)) FOR [ISUPDATED]
GO
ALTER TABLE [dbo].[usertype_tbl] ADD  CONSTRAINT [DF_userttype_tbl_issuperadmin]  DEFAULT ((0)) FOR [issuperadmin]
GO
ALTER TABLE [dbo].[usertype_tbl] ADD  CONSTRAINT [DF_usertype_tbl_inserteddate]  DEFAULT (getdate()) FOR [inserteddate]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1-Demography,2-condisionsandrisk,3-costandutilization,4-bcbsriprogram,5-patinetpanelalldata,6-returnreport' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'bcbsri_risk_report_tbl', @level2type=N'COLUMN',@level2name=N'type_of_sheet'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0-Recent,1-Inprogress,2-Completed' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'returnreport_data_tbl', @level2type=N'COLUMN',@level2name=N'currentstatus'
GO
ALTER DATABASE [dcmdatabase] SET  READ_WRITE 
GO
